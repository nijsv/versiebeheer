Studentnummer: 333736
Naam:Nijs van Duin

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# Nee

Waar worden Branches vaak voor gebruikt?
	# Om te comands te kunnen gebruiken

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2x

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# git status
 -Nieuwe Branch aanmaken
	# git branch 
 -Van Branch wisselen
	# git checkout ...
 -Branch verwijderen
	# git branch -d ...